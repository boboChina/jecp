package net.loyin.model.zh;

import net.loyin.jfinal.anatation.TableBind;

import com.jfinal.plugin.activerecord.Model;

@TableBind(name="zh_project")
public class Project extends Model<Project> {
	public static String tableName="zh_project";
	private static final long serialVersionUID = 1287150070505939605L;
	public static final Project dao=new Project();
}
