package net.loyin.ctrl.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import net.loyin.ctrl.AdminBaseController;
import net.loyin.jfinal.anatation.RouteBind;
import net.loyin.model.sys.Partment;
import net.loyin.pageModel.DataGrid;
import net.loyin.pageModel.TreeNode;
/**
 * 组织机构设置
 * @author loyin
 *2013-3-31 上午11:14:50
 */
@RouteBind(path="partment")
public class PartmentCtrl extends AdminBaseController<Partment> {
	public PartmentCtrl(){
		this.tableName=Partment.tableName;
		modelClass=Partment.class;
	}
	/**树形表格*/
	public void treeDataGrid() {
		DataGrid dg=new DataGrid();
		List<Record> list= Db.find("select a.*,p.cname pcname,concat(e.fullname,'(',e.userno,')')FZRNAME from "+this.tableName+" a left join "+this.tableName+" p on p.id=a.pid  left join hr_employee e on e.id=a.fzr ");
		List<Map<String,Object>> list1=new ArrayList<Map<String,Object>>();
		for(Record r:list){
			Map<String,Object> d=new HashMap<String,Object>();
			for(String col:r.getcolumnNames()){
				d.put(col,r.get(col));
			}
			Long pid=r.getLong("pid");
			if(pid!=null&&pid!=0)
			d.put("_parentId",pid);//tree datagrid 需要使用此字段作为父节点关联
			list1.add(d);
		}
		dg.setRows(list1);
		dg.setTotal(list1.size());
		this.renderJson(dg);
	}
	
	@Override
	public void combotree() {
		List<Record> dataList = Db.find("select id,cname,pid from "+this.tableName);
		List<TreeNode> list = new ArrayList<TreeNode>();
		TreeNode rootNode = new TreeNode();
		fillDepartmentTree(dataList, rootNode,"cname",true,null);
		list.add(rootNode);
		this.renderJson_(list);
	}
	
	public void fillDepartmentTree(List<Record> dataList,TreeNode pnode,String cname,Boolean canChk,List<Long>ckidList) {
		List<TreeNode> childelist = new ArrayList<TreeNode>();
		if(null==pnode.getId()){
			pnode.setId(dataList.get(0).getLong("id"));
			pnode.setText(dataList.get(0).getStr(cname));
			pnode.setIconCls("icon-home");
			pnode.setCanChk(false);
		}
		for (Record m : dataList) {
			Long pid_ = m.getLong("pid");
			if (pnode.getId() == pid_ && pid_!=0) {
				Long id = m.getLong("id");
				TreeNode nodechild = new TreeNode();
				nodechild.setId(id);
				nodechild.setText(m.getStr(cname));
				nodechild.setCanChk(canChk);
				childelist.add(nodechild);
				if(ckidList!=null&&ArrayUtils.contains(ckidList.toArray(), id))
				nodechild.setChecked(true);
				fillTree(dataList, nodechild,cname,canChk,ckidList);
			}
		}
		if (childelist.isEmpty() == false && childelist.size() > 0) {
			pnode.setChildren(childelist);
		}
	}
}
