package net.loyin.ctrl.zh;

import java.util.ArrayList;
import java.util.List;

import net.loyin.ctrl.AdminBaseController;
import net.loyin.jfinal.anatation.RouteBind;
import net.loyin.model.zh.Project;
import net.loyin.pageModel.DataGrid;

import com.jfinal.plugin.activerecord.Page;

@RouteBind(path="project")
public class ProjectCtrl extends AdminBaseController<Project> {
	public ProjectCtrl(){
		this.tableName=Project.tableName;
		modelClass=Project.class;
	}
	
	@Override
	public void dataGrid() {
		List<Object> param = new ArrayList<Object>();
		StringBuffer where = new StringBuffer(" where 1=1 ");
		/** 添加查询字段条件*/
		try{
			String endDate=this.getPara("endDate");
			if(endDate!=null && !"".equals(endDate)){
				where.append(" and g.addtime <=?");
				param.add(endDate+" 23:59:59");
			}
			String startDate=this.getPara("startDate");
			if(startDate!=null && !"".equals(startDate)){
				where.append(" and g.addtime >=?");
				param.add(startDate+" 00:00:00");
			}
			String project=this.getPara("project");
			if(project!=null && !"".equals(project)){
				where.append(" and project like '%"+project+"%'");
			}
			String name=this.getPara("name");
			if(name!=null && !"".equals(name)){
				where.append(" and name like '%"+name+"%'");
			}
		}catch(Exception e){}
		String sortName = this.getPara("sort");
		String sortOrder = this.getPara("order", "desc");
		if (sortName != null) {
			where.append(" order by ");
			where.append(sortName);
			where.append(" ");
			where.append(sortOrder);
		}else{
			where.append(" order by id desc ");
		}
		Page<Project> p=Project.dao.paginate(this.getParaToInt("page",1),this.getParaToInt("rows",20),"select * ","from "+tableName+where.toString(),param.toArray());
		DataGrid dg=new DataGrid();
		dg.setRows(p.getList());
		dg.setTotal(p.getTotalRow());
		this.renderJson(dg);
	}
}