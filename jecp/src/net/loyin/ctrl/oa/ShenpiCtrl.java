package net.loyin.ctrl.oa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import net.loyin.ctrl.AdminBaseController;
import net.loyin.jfinal.anatation.RouteBind;
import net.loyin.model.oa.Shenpi;
import net.loyin.pageModel.DataGrid;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

@RouteBind(path = "shenpi")
public class ShenpiCtrl extends AdminBaseController<Shenpi> {
	public ShenpiCtrl() {
		this.tableName = Shenpi.tableName;
		modelClass = Shenpi.class;
	}
	public void dataGrid() {
		List<Object> param = new ArrayList<Object>();
		StringBuffer where = new StringBuffer(" 1=1");
		String content=this.getPara("title");
		if(content!=null&&!"".equals(content)){
			where.append(" and content like ?");
			param.add("%"+content+"%");
		}
		Page<Record> p = Db.paginate(this.getParaToInt("page", 1), this.getParaToInt("rows", 20), "select * ", "from " + this.tableName + " where"+ where.toString(), param.toArray());
		DataGrid dg = new DataGrid();
		dg.setRows(p.getList());
		dg.setTotal(p.getTotalRow());
		this.renderJson(dg);
	}
	
	public void view(){
		Long id= this.getParaToLong(0,0L);
		if(id!=null&&id!=0L){
			Model<Shenpi> r=get(id);
			this.setAttr("po",r);
		}else{
			this.setAttr("po",new Record());
		}
	}
	
	public void edit(){
		Long id = this.getParaToLong(0, 0L);
		if (id != 0L) {
			po = get(id);
		}else{
			po = new Shenpi();
		}
		this.setAttr("po",po);
    }
	
	public void save(){
		try {
			Model<Shenpi> m = this.getModel();
			Long id = m.getLong("ID");
			Long uid=getCurrentUserId();
			Date now=new Date();
			if (id != null && id != 0) {
				m.update();
			} else {
				m.set("author",uid);		//创建人
				m.set("addtime",now);		//创建时间
				m.save();
			}
			id = m.getLong("ID");
			rendJson_(true, "保存成功！", id,new Random().nextLong());
		} catch (Exception e) {
			log.error("保存异常", e);
			rendJson_(false, "保存异常！");
		}
	}
	
}