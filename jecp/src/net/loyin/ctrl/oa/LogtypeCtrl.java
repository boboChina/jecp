package net.loyin.ctrl.oa;

import java.util.List;

import net.loyin.ctrl.AdminBaseController;
import net.loyin.jfinal.anatation.RouteBind;
import net.loyin.model.oa.LogType;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

@RouteBind(path = "logtype")
public class LogtypeCtrl extends AdminBaseController<LogType> {
	public LogtypeCtrl() {
		this.tableName = LogType.tableName;
		modelClass = LogType.class;
	}
	
	public void combobox(){
		List<Record> list=Db.find("select * from "+this.tableName);
		this.renderJson(list);
	}
	
}