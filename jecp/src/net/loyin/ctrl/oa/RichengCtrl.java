package net.loyin.ctrl.oa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import net.loyin.ctrl.AdminBaseController;
import net.loyin.jfinal.anatation.RouteBind;
import net.loyin.model.oa.Richeng;
import net.loyin.pageModel.DataGrid;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

@RouteBind(path = "richeng")
public class RichengCtrl extends AdminBaseController<Richeng> {
	public RichengCtrl() {
		this.tableName = Richeng.tableName;
		modelClass = Richeng.class;
	}
	public void dataGrid() {
		List<Object> param = new ArrayList<Object>();
		StringBuffer where = new StringBuffer(" 1=1");
		String theme=this.getPara("title");
		if(theme!=null&&!"".equals(theme)){
			where.append(" and theme like ?");
			param.add("%"+theme+"%");
		}
		Page<Record> p = Db.paginate(this.getParaToInt("page", 1), this.getParaToInt("rows", 20), "select * ", "from " + this.tableName + " where"+ where.toString(), param.toArray());
		DataGrid dg = new DataGrid();
		dg.setRows(p.getList());
		dg.setTotal(p.getTotalRow());
		this.renderJson(dg);
	}
	
	public void view(){
		Long id= this.getParaToLong(0,0L);
		if(id!=null&&id!=0L){
			Model<Richeng> r=get(id);
			this.setAttr("po",r);
		}else{
			this.setAttr("po",new Record());
		}
	}
	
	public void edit(){
		Long id = this.getParaToLong(0, 0L);
		if (id != 0L) {
			po = get(id);
		}else{
			po = new Richeng();
		}
		this.setAttr("po",po);
    }
	
	public void save(){
		try {
			Model<Richeng> m = this.getModel();
			Long id = m.getLong("ID");
			Long uid=getCurrentUserId();
			Date now=new Date();
			if (id != null && id != 0) {
				m.update();
			} else {
				m.set("author",uid);//创建人
				m.set("addtime",now);//创建时间
				m.save();
			}
			id = m.getLong("ID");
			rendJson_(true, "保存成功！", id,new Random().nextLong());
		} catch (Exception e) {
			log.error("保存异常", e);
			rendJson_(false, "保存异常！");
		}
	}
	
	public void oper(){
		int id=this.getParaToInt("id");
		int status=this.getParaToInt("status");
		try{
			if(id> 0 && status >=0 && status <=5){
				Db.update("update oa_richeng set status=? where id=?",status,id);
				rendJson_(true, "状态设置成功！", id);
			}else{
				rendJson_(false, "未提交完整数据！");
			}
		} catch (Exception e) {
			log.error("保存异常", e);
			rendJson_(false, "保存异常！");
		}
	}
	
}